<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('home_model', 'hm');
	}
	public function index()
	{
		$data = array(
			'slider' 		=> $this->hm->getSlider(),
			'keunggulan'    => $this->hm->getKeunggulan(),
			'review'     	=> $this->hm->getReview(),
			'product'     	=> $this->hm->getProduct(),
			'testimoni'     => $this->getTestimoni(),
			'contact'       => $this->hm->getContact(),
			'socmed'     	=> $this->hm->getSocmed(),
			'driver'     	=> $this->hm->getDriver(),
			'coordinate'    => $this->get_coordinate_map(),
			'location'     	=> $this->get_map_location(),
			'optionDriver'  => $this->hm->get_dropdown_product(),
			'content'		=> 'home_content',
			'header'		=> 'frontend/layout/header'
		);
		
		$this->load->view('frontend/layout/main', $data, FALSE);
	}

	public function get_default_email()
	{
		return  getField('master_data', 'namaData', array('kodeCategory'=>'EML', 'statusData'=> 'y'));
	}

	public function send_message($value='')
	{
		$post = $this->input->post();

		if ($post) {
			$post['createBy'] = 'Visitor';
			$post['createDate'] = date('Y-m-d h:i:s');
			$this->hm->post_message($post);
			$this->email($post);
		}
	}

	public function email($post)
	{
		$this->load->library('mail');
		$getEmail = $this->hm->getContact();
		$this->mail->send($this->get_default_email(), $post['contactUsSubject'], $post['contactUsMessage']. '<br> From : <b>'. $post['contactUsEmail'] .'</b>');
	}

	public function getTestimoni($value='')
	{
		$value = $this->hm->getTestimoni();
		foreach ($value as $key => $val) {
			$value[$key]->testimoniUserPicture = $this->generate_image_testimoni($val->testimoniUserPicture);
		}
		return $value;
	}

	public function generate_image_testimoni($image='')
	{
		if ($image) {
			return base_url().'assets/uploads/testimoni/'.$image;
		}
		else{
			return base_url().'assets/uploads/testimoni/default.png';
		}
	}

	public function get_map_location($value='')
	{
		$getData = $this->hm->getBranch();

		$location = array();
		foreach ($getData as $key => $value) {
			$location[$key] = array(
				'locationName' => str_replace(" ", "",strtolower($value->branchName)),
				'info' 	   	   => '<br><strong>'.$value->branchName.'</strong><br>'. $value->branchAddress.'<br><a href="'.$value->branchLink.'"> Get Direction</a>',
				'lat' 	   	   => $value->branchXAxis,
				'long'		   => $value->branchYAxis,
			);
		}
		return $location;
	}

	public function get_coordinate_map($value='')
	{
		$getData = $this->hm->getBranch();

		$coordinate =  array();
		foreach ($getData as $key => $value) {
			$coordinate[$key]  = "[".str_replace(" ", "",strtolower($value->branchName)).".info, ".str_replace(" ", "",strtolower($value->branchName)).".lat, ".str_replace(" ", "",strtolower($value->branchName)).".long, ".$key."],";
		}
		return $coordinate;
	}

	public function get_product($value='')
	{
		$post = $this->input->post();
		header('Content-Type: application/x-json; charset=utf-8');
		$preData = $this->hm->getProduct($post['productId']);
		$data = $preData[0]->productDriver;
		echo json_encode($data);
	}


}

/* End of file Bulletin.php */
/* Location: ./application/modules/frontend/bulletin/controllers/Bulletin.php */