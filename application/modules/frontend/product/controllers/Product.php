<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('product_model', 'hm');
	}

	public function index($slug='')
	{
		$search = @$this->input->get('search');
		$search = strtolower(str_replace(" ", "-", $search));
		$page = @$this->input->get('page');

		$data = array(
			'contact'       => $this->hm->getContact(),
			'socmed'     	=> $this->hm->getSocmed(),
			'totalPage'     => ceil($this->hm->count_product()/9),
			'header'		=> "frontend/layout/header_scroll",
			'content'		=> "list",
		);

		if ($page && $page !='1') {
			$data['product'] = $this->hm->getProduct($search,($page-1)*9);
		}
		else{
			$data['product'] = $this->hm->getProduct($search,0);
		}
		if ($search!='') {

			$data['product'] 	= $this->hm->getProduct($search);
			$data['paging'] = true;
		}

		if ($slug) {
			$data['product'] = $this->hm->getProduct($slug);
			$data['content'] = 'detail';
			if ($data['product']) {
				$data['content'] = 'detail';
			}
			else{
				redirect(base_url().'notfound/product');
			}
		}
		
		$this->load->view('frontend/layout/main', $data, FALSE);
	}

	public function send_message($value='')
	{
		$post = $this->input->post();

		if ($post) {
			$post['createBy'] = 'Visitor';
			$post['createDate'] = date('Y-m-d h:i:s');
			$this->hm->post_message($post);
		}
	}


}

/* End of file Bulletin.php */
/* Location: ./application/modules/frontend/bulletin/controllers/Bulletin.php */