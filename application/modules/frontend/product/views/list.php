		<section>
			<div id="home" class="slider-area">
				<div class="home-slider">
					
					<div class="pt-50 slider-item bg-img slide-bg-1 sm-no-padding" style="height: 90px">
						<div class="container">
							<div class="row flexbox-center">
								<div class="col-md-4 hidden-xs hidden-sm">
									<div class="p-30 text-center wow zoomIn optin-form">
										
									</div>
								</div>
								<div class="col-md-offset-1 col-md-6">
									<div class="banner-text sm-text-center wow fadeInDown">
										
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</section>
	</header>

	<section>
		<div class="container" style="padding: 70px 0">
			<h4>Daftar Produk</h4>
			<hr><br>
			<div class="row">
				<div class="col-md-9">
					<?php foreach ($product as $key => $value) { ?>					
					<div class="col-md-4" style="border: 3px solid white; height: 426px;overflow: hidden;">
						<div class="wrapper" style="border: 1px solid #c4c4c4;border-radius: 3px;padding:15px 14px 0 14px;">
							<center>
								<img src="<?=base_url().'assets/uploads/product/'. $value->productImage?>" height="210px">
							</center>
							<div style="margin: 10px -14px 0px -14px;padding: 15px; " class="row light-bg">
								<div class="col-md-12">
									<font size="4"><?=$value->productName?></font><br>
									<p style="font-size: 0.8em;line-height: 1.4em;margin-bottom: 10px">
										<?= substr($value->overviewCaption, 0, 74) ?>.<br>
										<a href="<?=base_url().'product/'.$value->productSlug?>" class="pull-left" style="font-size: 0.8em;"><i>Lihat Selengkapnya &rarr;</i></a>
									</p>
								</div>

								<div  style="font-size: 0.9em"  class="col-md-7">
									<p class="text-primary" style="margin-bottom: -5px;margin-top: 10px">
										<b>Rp <?=konversi_uang($value->productPrice-$value->productDiscount)?></b>
									</p>
									<small>  &nbsp; <s>Rp <?=konversi_uang($value->productPrice)?></s> </small>
								</div>
								<div class="col-md-5" style="margin-top: 10px;padding: 0">
									<a class="btn btn-round btn-primary   <?=$value->stockButton?>" href="<?=base_url().'order/'.$value->productSlug?>" style="padding: 5px 10px;margin-top: 10px"><i class="icofont icofont-cart-alt"></i> Buy</a>
									<a class="btn btn-round btn-primary  <?=$value->emptyButton?>"  href="#" style="padding: 5px 10px;margin-top: 10px"><i class="icofont icofont-cart-alt"></i> <font size="3">Habis</font></a>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<div class="col-md-12 text-right">
						<?php if(!@$paging){ ?>
						<ul class="pagination">
							<?php 
							$active='';
							$page = 1 ;
							if (@$this->input->get('page')) {
								$page = $this->input->get('page');
							}
							for ($i=1; $i <= $totalPage; $i++) { 
								( $i==$page ? $active='active' : $active='')
								?>
								<li class="<?=$active?>"><a href="<?=base_url().'product?page='.$i?>"><?=$i?></a></li>
								<?php } ?>
							</ul> 
							<?php } ?>
						</div>
					</div>
					<div class="col-md-3">
						<div class="col-md-12">
							<div class="form-group">
								<label for="exampleInputEmail1">Search </label>
								<hr style="margin-top: -5px;">
								<input name="orderPaymentName" class="form-control" id="search_text" value="<?= !empty($this->input->get('search')) ? $this->input->get('search') : ''?>" placeholder="Nama produk">
								<div class="col-md-6 col-md-offset-6" style="padding: 0">
									<button id="search" style="margin-top: 10px;padding: 7px 15px; " class="btn btn-round btn-red btn-submit"><i class="fa fa-search"></i> Cari</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>





			<div id="back-top">
				<a href="#top"><i class="icofont icofont-arrow-up circled-icon"></i></a>
			</div>


			<script type="text/javascript">
				$('#search').click(function(){
					param = $('#search_text').val();
					window.location.replace("<?=base_url().'product?search='?>"+param);
				});
				$('#search_text').change(function(){
					param = $('#search_text').val();
					window.location.replace("<?=base_url().'product?search='?>"+param);
				});
			</script>