<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

	function getProduct($param='',$page="")
	{
		if ($param) {
			$this->db->like('productSlug', $param);
		}

		if ($page) {
			$offset = $page;
		}


		$this->db->where('productStatus','t');
		$this->db->limit(9,$page);
		$data =  $this->db->get('product')->result();

		return  $this->generate_empty_button($data);		
	}

	public function generate_empty_button($data='')
	{
		foreach ($data as $key => $value) {
			if ($value->productStock==0) {
				$data[$key]->emptyButton="disabled";
				$data[$key]->stockButton="hidden";
			}
			else{
				$data[$key]->emptyButton="hidden";
				$data[$key]->stockButton="";
			}
		}
		return $data;
	}

	function getSocmed()
	{
		return $this->db->get('socmed')->result();
	}

	function getContact()
	{
		return $this->db->get('contact')->result();
	}

	function getProduct_PAGING($number,$offset){
		return $query = $this->db->get('product',$number,$offset)->result();		
	}

	function count_product($value='')
	{
		$this->db->select('count(productId) as count');
		$this->db->where('productStatus','t');
		$this->db->from('product');
		$data =  $this->db->get()->result();

		return $data[0]->count;
	}
}

/* End of file Model_content.php */
/* Location: ./application/modules/frontend/content/models/Model_content.php */