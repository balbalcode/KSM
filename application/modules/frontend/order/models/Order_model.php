<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

	function getProduct($param='')
	{
		if ($param) {
			$this->db->where('productSlug', $param);
		}
		return $this->db->get('product')->result();
	}

	function getSocmed()
	{
		return $this->db->get('socmed')->result();
	}

	function getContact()
	{
		return $this->db->get('contact')->result();
	}

	function getBranch()
	{
		return $this->db->get('master_branch')->result();
	}

	function getCountOtder($branchId)
	{
		if ($branchId) {
			$this->db->where('branchId', $branchId);
		}
		return $this->db->get('order')->result();
	}

	function save_order($data='')
	{
		$this->db->insert('order', $data);
		$insert_id = $this->db->insert_id();

		return  $insert_id;
	}

	function save_transaction($data='')
	{
		$this->db->insert('order_payment', $data);
		$insert_id = $this->db->insert_id();

		return  $insert_id;
	}

	function get_order($orderId='')
	{
		if ($orderId) {
			$this->db->where('orderId', $orderId);
			$this->db->select('*');
			$this->db->from('order a');
			$this->db->join('product b', 'a.productId = b.productId', 'left');
		}
		return $this->db->get()->result();
	}


}

/* End of file Model_content.php */
/* Location: ./application/modules/frontend/content/models/Model_content.php */