		<section>
			<div id="home" class="slider-area">
				<div class="home-slider">
					<?php foreach ($slider as $key => $value) {  ?>
					<div class="pt-100 slider-item bg-img slide-bg-1 sm-no-padding">
						<div class="container">
							<div class="row flexbox-center">
								<div class="col-md-4 hidden-xs hidden-sm">
									<div class="p-30 text-center wow zoomIn optin-form">
										<img src="<?=base_url().'/assets/uploads/slider/'.$value->sliderImage?>" class="img-responsive">
									</div>
								</div>
								<div class="col-md-offset-1 col-md-6">
									<div class="banner-text sm-text-center wow fadeInDown">
										<h1><?=$value->sliderName?></h1>
										<p><?=$value->sliderDetail?></p>
										<div class="btn-set">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</section>
	</header>
	<section>
		<div class="padding-medium light-bg text-center featured-area" id="feature">
			<div class="container">
				<div class="row">
					<?php foreach ($keunggulan as $key => $value) { ?>
					<div class="col-md-3 col-sm-6 light-bg red-hover">
						<div class="featured-item">
							<h2><i class="fa <?=$value->advantageIcon?> circled-icon"></i></h2>
							<h3><?=$value->advantageName?></h3>
							<p><?=$value->advantageCaption?>.</p>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="padding-big bg-white" id="overview">
			<div class="container text-center">
				<div class="row" id="multiple-items">
					<?php foreach ($product as $key => $value) { ?>
					<div class="col-md-3 col-sm-5" style="border-left:10px solid white">
						<div class="p-30   single-author" style="border:1px solid #c9c4c4">
							
							<div style="padding: 30px;background: white;" >
								<img src="<?= base_url().'assets/uploads/product/thumb/'.$value->productImage ?>" class="img-responsive" alt="Author">
							</div>
							
							<h3 class="primary-color mb-30"><?=$value->productName ?></h3>
							<p  style="line-height: 1.4em"><?=$value->overviewCaption ?>.</p>
							<div class="row">
								<div class="col-md-6" style="padding: 0;padding-right: 5px">
									<a href="<?=base_url().'product/'.$value->productSlug?>" target="_blank" class="btn btn-round btn-red btn-download" style="padding: 4px 0px;width: 100%;font-size: 0.9em"> <i class="icofont  icofont-eye-alt" ></i> Detail</a>
								</div>
								<div class="col-md-6" style="padding: 0;padding-left: 5px">
									<a href="#" target="_blank" class="btn btn-round btn-red btn-download" style="padding: 4px 0px;width: 100%;font-size: 0.9em"> <i class="icofont icofont-cart-alt" ></i> Buy</a>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<br>
				<a href="<?=base_url().'product'?>" class="btn btn-round btn-red btn-submit">See All Products</a>

			</div>
		</div>
	</section>
	
	
	<section>
		<div class="bg-img video-area" id="review">
			<div class="container">
				<div class="row flexbox-center xs-no-flexbox">
					<div class="col-sm-3">
						<div class="text-center video-play-icon">
							<a href="<?=$review[0]->reviewVideo?>" class="popup-youtube">
								<i class="icofont icofont-ui-play circled-icon"></i>
							</a>
						</div>
					</div>
					<div class="col-md-offset-3 col-md-6 col-sm-9">
						<div class="section-heading">
							<h2 class="section-title user-activity-title"><?=$review[0]->reviewCaption?></h2>
							<p><?=$review[0]->reviewDetail?></p>
						</div>
						<br>
						<a href="<?=$review[0]->reviewVideo?>" class="popup-youtube"> Watch Video &rarr;</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	
	
	<section>
		<div class="padding-big pt-70 customer-feedback" id="feedback">
			<div class="container text-center">
				<div class="row">
					<div class="col-sm-offset-2 col-sm-8">
						<div class="mb-65 section-heading">
							<h2 class="section-title">Testimonials our <strong class="primary-color">Customers</strong></h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-offset-3 col-md-6">
						<div class="feedback-slider">
							<?php 
							$last = sizeof($testimoni)-1;
							foreach ($testimoni as $key => $value) { ?>
							<div class="p-60 pt-0 feedback-slider-item">
								<img src="<?=base_url().'assets/uploads/testimoni/'.$value->testimoniUserPicture?>" class="center-block img-circle" alt="Customer Feedback">
								<h3 class="customer-name"><?=$value->testimoniUserName?></h3>
								<p><?=$value->testimoniValue?></p>
								<span class="light-bg customer-rating" data-rating="5">
									5
									<i class="icofont icofont-star"></i>
								</span>
							</div>				
							<?php } ?>			
						</div>
						
						<div class="feedback-slider-thumb hidden-sm hidden-xs">
							<div class="thumb-prev">
								<span>
									<img src="<?=base_url().'assets/uploads/testimoni/'.@$testimoni[1]->testimoniUserPicture?>" alt="Customer Feedback">
								</span>
								<span class="light-bg customer-rating">
									5
									<i class="icofont icofont-star"></i>
								</span>
							</div>
							<div class="thumb-next">
								<span>
									<img src="<?=base_url().'assets/uploads/testimoni/'.@$testimoni[$last]->testimoniUserPicture?>" alt="Customer Feedback">
								</span>
								<span class="light-bg customer-rating">
									5
									<i class="icofont icofont-star"></i>
								</span>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>
	
	
	<section>
		<div class="padding-big bg-img slide-bg-1 author-area" id="driver" style="padding-bottom: 0px">
			<div class="container">
				<div class="row">					
					<?php foreach ($driver as $key => $value) {  ?>
					
					<div class="col-md-offset-1 col-md-6">
						<div class="banner-text sm-text-center wow fadeInDown">
							<h1><?=$value->driverCaption?></h1>
							<p><?=$value->driverDetail?></p>
							<div class="col-md-5">
								<select class='form-control search-select' data-placeholder='Pilih Device' name='device-list' id='device' style='color:white'>
									<option></option>
									<?php foreach ($optionDriver as $key => $optValue) { ?>
									<option value="<?=$key?>"><?=$optValue?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-7" >
								<a id="download_link" target="_blank" style="padding: 11px;font-size: 1em" class="disabled btn btn-round btn-transparent">&nbsp; &nbsp; &nbsp; Download Driver <i class="icofont icofont-cloud-download"></i> </a>
								
							</div>

						</div>
					</div>
					<div class="col-md-4 hidden-xs hidden-sm" style="padding: 0">
						<div class="wow zoomIn optin-form" style="padding: 0;margin-bottom: -14px;">
							<img src="<?=base_url().'/assets/uploads/driver/'.$value->driverImage?>">
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>


	<section>
		<div class="padding-big contact-area" id="contact">
			<div class="container">
				<div class="row">

					<div class="col-md-offset-1-and-half col-md-6">
						asd
					</div>
					<div class="col-md-4">
						<h4 class="mb-30">Need Help ? <strong class="primary-color">Contact</strong></h4>
						<div >
							<div class="mb-15">
								<label for="email" class="">Email</label>
								<input name="contactUsEmail" type="email" class="form-control" id="email" placeholder="Email">
							</div>
							<div class="mb-15">
								<label for="sub" class="">Subject</label>
								<input name="contactUsSubject" type="text" class="form-control" id="subject" placeholder="Subject">
							</div>
							<div class="mb-15">
								<label for="message" class="">Message</label>
								<textarea  name="" class="form-control" id="message" placeholder="Your Message"></textarea>
							</div>
							<div class="col-md-offset-6 col-md-6 text-right">
								<button id="send_message"  class="btn btn-round btn-red btn-submit">Submit</button>
							</div>
							<div class="mb-15">
								Connect To
								<ul class="social-icons">
									<?php foreach ($socmed as $key => $value) {  ?>
									<li style="height: 35px;width: 35px;text-align: center;background: #ff647a;border-radius: 17px;"><a href="<?=$value->target?>" target="_blank"><i class=" fa <?=$value->socmedIcon?> circled-icon"></i></a></li>
									<?php } 		?>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-offset-1 col-md-10">
						<div class="contact-form clearfix">

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<div id="back-top">
		<a href="#top"><i class="icofont icofont-arrow-up circled-icon"></i></a>
	</div>


	<script type="text/javascript">
		$(document).ready(function() {
			$(".search-select").select2({
				placeholder : 'Pilih Data...',
				minimumResultsForSearch: 10,
				allowClear: true,
			});
		});
		$('#multiple-items').slick({
			infinite: true,
			slidesToShow: 4,
			slidesToScroll: 2,
			autoplay: true,
			autoplaySpeed: 1500,
			arrows:true,
		});

		$('#device').click(function() {
			link = $('#device').val();
			$('#download_link').removeClass('disabled');
			$('#download_link').prop('href', link);

		});

		$('#send_message').click(function() {
			email   = $('#email').val();
			subject = $('#subject').val();
			message = $('#message').val();

			message = {
				contactUsEmail:email,
				contactUsSubject:subject,
				contactUsMessage:message,
			}

			swal({
				type :"success",
				title: "Sent",
				text: "Thanks!. We'll reply your contact to your email for next time",
			}, function(){   
				save(message)

			});

		});

		function save(data) {
			$.ajax({
				type: "POST",
				data: data,
				url: "<?=base_url().getModule().'/'.getController().'/send_message'?>",
				success: function(data) {
					window.location.href ="<?=base_url()?>";
				}
			});
		}

	</script>