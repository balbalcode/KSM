		<section>
			<div id="home" class="slider-area">
				<div class="home-slider">
					
					<div class="pt-50 slider-item bg-img slide-bg-1 sm-no-padding" style="height: 90px">
						<div class="container">
							<div class="row flexbox-center">
								<div class="col-md-4 hidden-xs hidden-sm">
									<div class="p-30 text-center wow zoomIn optin-form">
										
									</div>
								</div>
								<div class="col-md-offset-1 col-md-6">
									<div class="banner-text sm-text-center wow fadeInDown">
										
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</section>
	</header>

	<section>
		<div class="container" >
			<div class="row " style="padding: 100px 0;z-index: 9">
				<div class="">
					<div class="col-md-8 col-md-offset-2" >
						<center>
							<h2>
							Bukti Pembayaran </h2> <h4 style="margin-top: 0px;font-size: 1em">Harap lengkapi form di bawah untuk memproses pesanan.</h4>
							
							<hr style="margin-top: 5px">
						</center>
						<form enctype="multipart/form-data" action="<?=base_url().getModule().'/transaction'?>" method="post" style="border: 1px solid #c4c4c4;padding: 30px;margin-top: -20px" >
							<div class="form-group">
								<label for="exampleInputEmail1">Order Invoice </label>
								<input name="orderInvoice" class="form-control" id="exampleInputEmail1" placeholder="Order Invoice" required="">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Nama </label>
								<input name="orderPaymentName" class="form-control" id="exampleInputEmail1" placeholder="Nama" required="">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Email</label>
								<input name="orderPaymentEmail" type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required="">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Bukti Pembayaran</label>
								<?=input_file_image('orderPayment','','required')?>
							</div>
							<div class="form-group text-center" style="margin-top: 60px">
								<button id="send_message" style="padding: " class="btn btn-round btn-red btn-submit">Submit</button>
								<a href="<?=base_url()?>" class="btn btn-round btn-white btn-submit" style="border: 1px solid #ec3851;padding: ">Cancel</a>
							</div>
						</form>

					</div>

					

				</div>
			</div>
		</div>
	</section>

	<script type="text/javascript">
		$(document).ready(function() {
			<?=@$notifications?>
		});
	</script>