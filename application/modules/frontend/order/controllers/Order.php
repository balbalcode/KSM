<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('order_model', 'hm');
	}

	public function index($slug='')
	{
		$data = array(
			'product'     	=> $this->hm->getProduct(),
			'socmed'     	=> $this->hm->getSocmed(),
			'contact'       => $this->hm->getContact(),
			'header'		=> "frontend/layout/header_scroll",
			'content'		=> "order_content"
		);

		if ($slug) {
			$data['branch'] = $this->hm->getBranch($slug);
			$data['product'] = $this->hm->getProduct($slug);
			if ($data['product']) {
				$data['content'] = 'order_content';
			}
			else{
				redirect(base_url().'notfound/product');
			}
		}
		$this->load->view('frontend/layout/main', $data, FALSE);
	}

	public function post($value='')
	{
		$post = $this->input->post();

		$post['orderCode']		= "INV/0".$post['branchId']."/".$this->make_order_code($post['branchId']);
		$post['uniqCode']		= $this->random_digits('3');
		$post['orderStatus']	= "Incoming";

		if ($post) {
			$post['createBy'] = 'Visitor';
			$post['createDate'] = date('Y-m-d h:i:s');
			$insId = $this->hm->save_order($post);
		}
		
		$this->struck(base64_encode($insId));
	}


	public function transaction($value='')
	{
		$post = $this->input->post();



		if (@$_FILES['orderPayment']['name']!='') 
		{
			$this->load->library('uploads');
			$dir = "assets/uploads/order/payment/";
			$data = $this->uploads->upload_image('orderPayment',$dir);
			$name = $data['file_name'];
			if(@$post['orderPayment']=="")
			{
				$post['orderPayment'] = $name; 
			}

			$post['createBy'] = 'Visitor';
			$post['createDate'] = date('Y-m-d h:i:s');
			$insId = $this->hm->save_transaction($post);

			if ($insId) {
				$this->payment('success');
			}
		}
		else{
			$this->payment('fail');
		}
	}

	public function payment($notifications='')
	{
		$data = array(
			'socmed'     	=> $this->hm->getSocmed(),
			'contact'       => $this->hm->getContact(),
			'header'		=> "frontend/layout/header_scroll",
			'content'		=> "order_payment"
		);

		if ($notifications == 'success') {
			$data['notifications'] = "swal({title : 'Payment Success', type : 'success', text : 'We will contact your email or your phone number for next steps.'});";
		}
		elseif  ($notifications == 'fail') {
			$data['notifications'] = "swal({title : 'Payment Decline', type : 'warning', text : 'Please complete the form with correct data!'});";
		}

		$this->load->view('frontend/layout/main', $data, FALSE);
	}

	public function struck($value='')
	{
		$id = base64_decode($value);
		$data = array(
			'socmed'     	=> $this->db->get('socmed')->result(),
			'struck'     	=> $this->hm->get_order($id),
			'header'		=> "none",
			'non_footer'	=> true,
			'content'		=> 'order_struck',
			
		);
		$this->load->view('frontend/layout/main', $data, FALSE);

	}

	public function make_order_code($branchId='')
	{
		$orderCount = sizeof($this->hm->getCountOtder($branchId));

		$code = "INV/0".$branchId."/".$orderCount+1;
		return $code;
	}

	public function random_digits($digits=""){
		if ($digits==3) {
			$val = rand(0, 2).rand(0, 9).rand(0, 9);
		} else {
			$val = str_pad(rand(0, pow(10, $digits)-1), $digits, '1', STR_PAD_LEFT);
		}
		
		return $val;
	}
}

/* End of file Bulletin.php */
/* Location: ./application/modules/frontend/bulletin/controllers/Bulletin.php */