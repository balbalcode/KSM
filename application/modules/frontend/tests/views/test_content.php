	<!-- /Main Header including Banner Ends -->
	<!-- Featured Section Starts -->
	<section>
		<div class="padding-big light-bg author-area" id="author">
			<div class="container text-center">
				<div class="row">
					<div class="col-sm-offset-2 col-sm-8">
						<div class="mb-65 xs-no-margin section-heading">
							<h2 class="section-title">Meet With <strong class="primary-color">Our Authors</strong></h2>
						</div>
					</div><!-- /End col -->
				</div><!-- /End row -->
				<div class="row">					
					<!-- single author item starts -->
					<div class="col-md-offset-1-and-half col-md-3 col-sm-4">
						<div class="p-35 img-rounded gray-border white-bg single-author">
							<img src="img/demo-3/author-1.jpg" class="img-circle" alt="Author">
							<h3 class="primary-color author-name">Jhon Alex</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							<ul class="text-center social-icons">
								<li><a href="#" target="_blank"><i class="icofont icofont-social-facebook circled-icon"></i></a></li>
								<li><a href="#" target="_blank"><i class="icofont icofont-social-twitter circled-icon"></i></a></li>
								<li><a href="#" target="_blank"><i class="icofont icofont-social-google-plus circled-icon"></i></a></li>
								<li><a href="#" target="_blank"><i class="icofont icofont-social-pinterest circled-icon"></i></a></li>
							</ul>
						</div><!-- /End single-author -->
					</div>
					<!-- /single author item ends -->
					<!-- single author item starts -->
					<div class="col-md-3 col-sm-4">
						<div class="p-35 img-rounded gray-border white-bg single-author">
							<img src="img/demo-3/author-2.jpg" class="img-circle" alt="Author">
							<h3 class="primary-color author-name">Alex Jhon</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							<ul class="text-center social-icons">
								<li><a href="#" target="_blank"><i class="icofont icofont-social-facebook circled-icon"></i></a></li>
								<li><a href="#" target="_blank"><i class="icofont icofont-social-twitter circled-icon"></i></a></li>
								<li><a href="#" target="_blank"><i class="icofont icofont-social-google-plus circled-icon"></i></a></li>
								<li><a href="#" target="_blank"><i class="icofont icofont-social-pinterest circled-icon"></i></a></li>
							</ul>
						</div><!-- /End single-author -->
					</div>
					<!-- /single author item ends -->
					<!-- single author item starts -->
					<div class="col-md-3 col-sm-4">
						<div class="p-35 img-rounded gray-border white-bg single-author">
							<img src="img/demo-3/author-3.jpg" class="img-circle" alt="Author">
							<h3 class="primary-color author-name">Janni Mith</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							<ul class="text-center social-icons">
								<li><a href="#" target="_blank"><i class="icofont icofont-social-facebook circled-icon"></i></a></li>
								<li><a href="#" target="_blank"><i class="icofont icofont-social-twitter circled-icon"></i></a></li>
								<li><a href="#" target="_blank"><i class="icofont icofont-social-google-plus circled-icon"></i></a></li>
								<li><a href="#" target="_blank"><i class="icofont icofont-social-pinterest circled-icon"></i></a></li>
							</ul>
						</div><!-- /End single-author -->
					</div>
					<!-- /single author item ends -->					
				</div><!-- /End row -->
			</div><!-- /End container -->
		</div><!-- /End author-area -->
	</section>
	<!-- /Author Section Ends -->
	<!--  Contact Section Starts -->
	<section>
		<div class="padding-big contact-area" id="contact">
			<div class="container">
				<div class="row text-center">
					<div class="col-sm-offset-2 col-sm-8">
						<div class="mb-50 section-heading">
							<h2 class="section-title">Need Help ? <strong class="primary-color">Contact</strong></h2>
							<p>Lorem ipsum dolor amet consectetur adipisicing eiusmod. Lorem ipsum dolor consectetur adipisicing  ipsum dolor amet consectetur adipisicing eiusmod.</p>
						</div>
					</div><!-- /End col -->
				</div><!-- /End row -->
				<div class="row">
					<div class="col-md-offset-1 col-md-10">
						<div class="contact-form clearfix">
							<form action="index-2.html">
								<div class="contact-field field-one-third">
									<label for="name" class="sr-only">Name: </label>
									<input type="text" class="form-control" id="name" required="required" placeholder="Full Name">
								</div>
								<div class="contact-field field-one-third">
									<label for="email" class="sr-only">Email</label>
									<input type="email" class="form-control" id="email" required="required" placeholder="Email">
								</div>
								<div class="contact-field field-one-third pull-right">
									<label for="sub" class="sr-only">Subject</label>
									<input type="text" class="form-control" id="sub" required="required" placeholder="Subject">
								</div>
								<div class="contact-field">
									<label for="message" class="sr-only">Message</label>
									<textarea id="message" class="form-control" required="required" placeholder="Your Message"></textarea>
								</div>
								<div class="text-center">
									<button type="submit" class="btn btn-round btn-red btn-submit">Submit</button>
								</div>
							</form>
						</div><!-- /End contact-form -->						
					</div><!-- /End col -->
				</div><!-- /End row -->
			</div><!-- /End container -->
		</div><!-- /End contact-area -->
	</section>
	<!-- / Contact Section Ends -->
	<!-- Start Back to Top -->
	<div id="back-top">
		<a href="#top"><i class="icofont icofont-arrow-up circled-icon"></i></a>
	</div>