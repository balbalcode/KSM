<div class="container">

	<?= getBread() ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-border panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title"> Contact Us </h4>
				</div>
				<div class="panel-body">					
					<div class="row" style="margin-top:20px;">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<table id="datatable" class="table table-custom table-bordered">
								<thead>
									<tr>
										<th class="text-center">No</th>
										<th class="text-center">Email</th>
										<th class="text-center">Subjek</th>
										<th class="text-center" width="40%">Detail</th>
										<th class="text-center" width="8%">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($data as $key => $value) { ?>
									<tr>
										<td style="vertical-align:middle;" class="text-center"><?= $no++ ?></td>
										<td style="vertical-align:middle;"><?= $value['contactUsEmail'] ?></td>
										<td style="vertical-align:middle;"><?= $value['contactUsSubject'] ?></td>
										<td style="vertical-align:middle;"><?= $value['contactUsMessage'] ?></td>
										<td style="vertical-align:middle;text-align: center;">
											<?php if(hak_akses('update')){ ?>
											<a href="<?php echo base_url().getModule() ?>/<?php echo getController() ?>/reply/<?php echo $value['contactUsId'] ?>">
												<button data-toggle="tooltip" data-title="Balas Pesan" data-placement="bottom"   class="btn btn-icon waves-effect waves-light btn-inverse btn-xs m-b-5" data-attr="<?= $value['contactUsId'] ?>"><i class="fa fa-sign-in"></i> </button>
											</a>
											<?php } ?>
										</td>
									</tr>
									<?php
								}
								?>

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>