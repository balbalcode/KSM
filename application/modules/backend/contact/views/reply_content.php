
<div class="container">
	<?= getBread() ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel  panel-border panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Pesan </h3>
				</div>
				<div class="panel-body">
					<div class="row" style="padding: 45px;"> 
						<div class="col-sm-2 ">
							<h4>Pesan</h4><hr style="margin-top: -5px;">
						</div>
						<div class="col-md-12">
							<div class="form-group row">
								<label class="col-lg-2 col-sm-2 control-label">Pengirim </label>
								<div class="col-lg-9 col-sm-9">
									<?=$data[0]['contactUsEmail']?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-lg-2 col-sm-2 control-label">Subject </label>
								<div class="col-lg-9 col-sm-9">
									<?=$data[0]['contactUsSubject']?>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-lg-2 col-sm-2 control-label">Pesan </label>
								<div class="col-lg-9 col-sm-9">
									<?=$data[0]['contactUsMessage']?>
								</div>
							</div>
						</div>

					</div>
					<div class="row" style="padding: 0;margin: 0"> 
						<div class=" col-md-offset-1 col-md-10 ">
							<h4>Pesan</h4><hr style="margin-top: -5px;">
						</div>
						<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>/post_message">
							<input type="hidden" name="contactUsId" class="form-control" value="<?php echo ($data) ? $data[0]['contactUsId'] : "" ?>">
							<?php echo select_join_group('namaData','namaData,namaData','master_data','namaData','from',' Pengirim ',array('kodeCategory'=> 'EML', 'statusData'=>'y'),'','required','Pilih Email') ?>	 
							<?php echo input_text_group('contactUsEmail',' Penerima',(@$data[0]['contactUsEmail']) ? @$data[0]['contactUsEmail'] : set_value('contactUsEmail'),'Email Penerima ','required', array('readonly' => 'true')) ?>	
							<?php echo input_text_group('contactUsSubject','Subject','','Subject ','required') ?>
							<div class="form-group">
								<label class="col-lg-2 col-sm-2 control-label">Message</label>
								<div class="col-lg-9 col-sm-9">
									<?= form_textarea('emailMessage', (@$data[0]['emailMessage']) ? @$data[0]['emailMessage'] : set_value('emailMessage') , 'id="isi" class="form-control"'); ?>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
									&nbsp;
									<a href="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>" class="btn btn-inverse"><i class="fa fa-times"></i> Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><script type="text/javascript">
	$(document).ready(function() {
		tinymce.init({
			selector: '#isi',
			height: 300,
			menu: {
				file: {title: 'File', items: 'newdocument'},
				edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
				view: {title: 'View', items: 'visualaid'},
				format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
				table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
				tools: {title: 'Tools', items: 'spellchecker code'}
			}
		});
	});
</script>