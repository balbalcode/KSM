<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lists extends MY_Controller {

	public function index()
	{
		if(hak_akses('view') === FALSE){
			show_errPrivilege();
			exit();
		}
		$data['data'] = $this->model->get('product');
		$data['content'] = 'product_content';
		$this->load->view('backend/main',$data,FALSE);
	}

	public function add($id='',$param='')
	{

		if ($param!="" && $param == "delete") {
			$this->load->library('uploads');
			$getLogo = $this->model->get_where('product',array('productId'=>$id));
			
			$photo = $this->uploads->upload_image('productImage','assets/uploads/product');
			unlink('assets/uploads/product/'.$getLogo[0]['productImage']);
			unlink('assets/uploads/product/thumb/'.$getLogo[0]['productImage']);
			
			$post['productImage'] = $photo['file_name'];
			$this->model->update_data('product',$post,array('productId'=>$id));
			redirect(base_url(getModule().'/'.getController().'/add/'.$id),'refresh');
		}

		$data['other_js'] = array(
			'backend/plugins/tinymce/tinymce.min.js'
			);

		$data['content'] 		= 'product_add';
		$data['data'] 		  	= $this->model->get_where('product',array('productId'=> $id));
		$this->load->view('backend/main',$data,FALSE);
	}

	public function save()
	{
		$post = $this->input->post();

		$config = array(
			'field' => 'productSlug',
			'table' => 'product',
			'id' => 'productId',
			);
		$this->load->library('slug', $config);
		$post['productSlug']  = $this->slug->create_uri($post['productName']);
		$post['productPrice'] = $this->remove_dot($post['productPrice']);
		$post['productDiscount'] = $this->remove_dot($post['productDiscount']);

		if (@$_FILES['productImage']['name']!='') 
		{
			$this->load->library('uploads');
			$dir = "assets/uploads/product/";
			$data = $this->uploads->upload_image('productImage',$dir);
			$name = $data['file_name'];
			$this->uploads->resize_image($dir.$name,$dir.'thumb/','525','525',''.$name);
			if(@$post['productImage']=="")
			{
				$post['productImage'] = $name; 
			}
		}

		if (@$post['productId']) {
			if(hak_akses('update') === FALSE){
				show_errPrivilege();
				exit();
			}
			$post['updateDate'] = date('Y-m-d H:i:s');
			$post['updateBy'] = $this->session->userdata('usernameUser');
			$this->model->update_data('product',$post,array('productId'=>$post['productId']));
		} else {
			if(hak_akses('create') === FALSE){
				show_errPrivilege();
				exit();
			}
			$post['createDate'] = date('Y-m-d H:i:s');
			$post['createBy'] = $this->session->userdata('usernameUser');
			$this->model->insert_data('product',$post);
		}
		redirect(getModule().'/'.getController());
	}

	public function remove_dot($value='')
	{
		return str_replace(".", "", $value);
	}

	public function delete($id="")
	{
		$getLogo = $this->model->get_where('product',array('productId'=>$id));
		$this->model->delete_data('product', 'productId='.$id);
		unlink('assets/uploads/product/'.$getLogo[0]['productImage']);
		unlink('assets/uploads/product/thumb/'.$getLogo[0]['productImage']);
	}

}

/* End of file product.php */
/* Location: ./application/modules/setting/controllers/product.php */