
<div class="container">
	<?= getBread() ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel  panel-border panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Produk </h3>
				</div>
				<div class="panel-body">
					<div class="row"> 
						<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>/save">
							<input type="hidden" name="productId" class="form-control" value="<?php echo ($data) ? $data[0]['productId'] : "" ?>">
							<?php echo input_text_group('productName','Nama',(@$data[0]['productName']) ? @$data[0]['productName'] : set_value('productName'),'Nama Produk ','') ?>
							<?php echo input_text_group('productColor','Warna',(@$data[0]['productColor']) ? @$data[0]['productColor'] : set_value('productColor'),'Warna Produk ','') ?>
							<?php echo input_text_group('productWeight','Berat (Kg)',(@$data[0]['productWeight']) ? @$data[0]['productWeight'] : set_value('productWeight'),'Berat Produk ','') ?>
							<?php echo input_text_group('productStock','Stok',(@$data[0]['productStock']) ? @$data[0]['productStock'] : set_value('productStock'),'Stok Produk ','') ?>
							<?php echo input_text_group('overviewCaption','Overview',(@$data[0]['overviewCaption']) ? @$data[0]['overviewCaption'] : set_value('overviewCaption'),'Overview Produk ','') ?>
							<?php echo input_text_group('productDriver','Link Driver',(@$data[0]['productDriver']) ? @$data[0]['productDriver'] : set_value('productDriver'),'Driver Produk ','') ?>
							<?php echo input_text_group('productPrice','Harga (Rp)',(@$data[0]['productPrice']) ? konversi_uang(@$data[0]['productPrice']) : set_value('productPrice'),'Harga Produk ','', array('onkeyup'=> 'nominal(this)')) ?>
							<?php echo input_text_group('productDiscount','Diskon',(@$data[0]['productDiscount']) ? konversi_uang(@$data[0]['productDiscount']) : set_value('productDiscount'),'Diskon Produk ','', array('onkeyup'=> 'nominal(this)')) ?>
							<?php if (@$data[0]['productDiscount']) { ?>
							<div class="form-group" style="margin-top: -15px;">
								<label class="col-lg-2 col-sm-2 control-label"></label>
								<div class="col-lg-9 col-sm-9">
									<small><i>Total Harga Rp. <?=konversi_uang(@$data[0]['productPrice']-@$data[0]['productDiscount'])?> </i></small>
								</div>
							</div>		
							<?php } ?>
							<?php echo input_file_image_group('productImage','Gambar',!empty(@$data[0]['productImage']) ? base_url().'assets/uploads/product/thumb/'. @$data[0]['productImage'] : '' ,   array('data-title' => 'Hapus gambar','data-desc' => 'Apakah anda yakin ingin menghapus gambar ini?','data-confirm' => 'Berhasil di hapus','data-route' => base_url(getModule()."/".getController()."/add/".@$data[0]['productId']."/delete")), 'required')?>				
							<div class="form-group">
								<label class="col-lg-2 col-sm-2 control-label">Deskripsi</label>
								<div class="col-lg-9 col-sm-9">
									<?= form_textarea('productDescription', (@$data[0]['productDescription']) ? @$data[0]['productDescription'] : set_value('productDescription') , 'id="isi" class="form-control"'); ?>
								</div>
							</div>
							<?php echo input_radio_group('sliderStatus','Status',array('t'=>'Tampil','f'=>'Tidak Tampil'),(@$data[0]['sliderStatus']) ? @$data[0]['sliderStatus'] : 't' ,'required') ?>

							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
									&nbsp;
									<a href="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>" class="btn btn-inverse"><i class="fa fa-times"></i> Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><script type="text/javascript">
	$(document).ready(function() {
		tinymce.init({
			selector: '#isi',
			height: 300,
			theme: 'modern',
			file_browser_callback_types: 'image',
			image_title: true, 
			automatic_uploads: true,
			images_upload_url: 'upload_gambar',
			file_picker_callback: function(cb, value, meta) {
				var input = document.createElement('input');
				input.setAttribute('type', 'file');
				input.setAttribute('accept', 'image/*');

				console.log(input);

				input.onchange = function() {
					var file = this.files[0];

					var reader = new FileReader();
					reader.readAsDataURL(file);
					reader.onload = function () {
						var id = 'img' + (new Date()).getTime();
						var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
						var base64 = reader.result.split(',')[1];
						var blobInfo = blobCache.create(id, file, base64);
						blobCache.add(blobInfo);

						cb(blobInfo.blobUri(), { title: file.name });
					};
				};

				input.click();
			},
			images_upload_handler: function (blobInfo, success, failure) {
				var xhr, formData;
				var upload_url = '<?php echo base_url() ?><?php echo getModule() ?>/<?php echo getController() ?>/upload_gambar';
				xhr = new XMLHttpRequest();
				xhr.withCredentials = false;
				xhr.open('POST', upload_url);
				xhr.onload = function() {
					var json;

					if (xhr.status != 200) {
						failure('HTTP Error: ' + xhr.status);
						return;
					}
					json = JSON.parse(xhr.responseText);

					if (!json || typeof json.location != 'string') {
						failure('Invalid JSON: ' + xhr.responseText);
						return;
					}
					success(json.location);
					console.log("tesss" + json);
				};
				formData = new FormData();
				formData.append('gambar', blobInfo.blob(), blobInfo.filename());
				xhr.send(formData);
			},
			plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
			],
			toolbar1: 'insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
			toolbar2: 'undo redo | preview media | forecolor backcolor emoticons | codesample help',
			image_advtab: true,

			content_css: [
			'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
			'//www.tinymce.com/css/codepen.min.css'
			],
			table_default_attributes: {
				class: 'table table-bordered'
			}
		});
	});
</script>