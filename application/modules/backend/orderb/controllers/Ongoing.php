<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ongoing extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('orderb_model', 'om');
	}

	public function index()
	{
		$branch =  $this->session->userdata('branchUser');
		
		$data['order']   		= $this->om->get_filter_order($this->session->userdata('branchUser'),'Ongoing');
		$data['titleApprove']   = 'Success Order';
		$data['content'] 		= 'order_content';
		
		
		$this->load->view('backend/main',$data,FALSE);
	}

	public function detail($value='')
	{
		$data['data']   	= $this->model->join('order', 'product.*, order.*, master_branch.branchName', array(array('table'=> 'product', 'parameter' => 'order.productId=product.productId'),array('table'=> 'master_branch', 'parameter' => 'order.branchId=master_branch.branchId')), array('orderId'=> $value));
		$data['content'] 	= 'order_detail';

		$this->load->view('backend/main',$data,FALSE);
	}

	public function approve($id)
	{
		$updateData = array(
			'orderId' => $id,
			'orderStatus'=> 'Complete',
			'completeApprove' => $this->session->userdata('usernameUser'),
			'completeDate' => date('Y-m-d h:i:s')
		);

		$updateStock  = $this->om->update_stock($id);
		$updateOrder = $this->om->update_order($updateData);
		redirect(base_url().'/orderb/decline','refresh');
	}

	public function decline($id)
	{
		$updateData = array(
			'orderId' => $id,
			'orderStatus'=> 'Declined',
			'completeApprove' => $this->session->userdata('usernameUser'),
			'completeDate' => date('Y-m-d h:i:s')
		);

		$updateStock  = $this->om->update_stock($id);
		$updateOrder = $this->om->update_order($updateData);
		redirect(base_url().'/orderb/complete','refresh');
	}

	

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */