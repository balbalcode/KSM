<style type="text/css">
	.dt-button, .btn-group{
		margin: auto;
		margin-bottom: -45px;
	}
</style>
<div class="container">
	<?= getBread() ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-border panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title"> Order Report </h4>
				</div>
				<div class="row" style="margin: 30px 0 0 0; ">
					<p style="margin-left: 20px; font-size: 1.4em" class="text-primary">Filter : <br></p>
					<form class="col-md-8" action="<?=base_url().getModule().'/'. getController().'/request'?>" method="post">
						
						<div class="col-md-3">
							<?=select_join("orderStatus","orderStatus","order","orderStatus","status","","","","Pilih Status");?>
						</div>
						<div class="col-md-3">
							<?=select_join("year(createDate)","year(createDate)","order","year(createDate)","year","","","","Pilih Tahun");?>
						</div>
						<div class="col-md-3">
							<?=select_join("month(createDate)","month(createDate)","order","month(createDate)","month","","","","Pilih Bulan");?>
						</div>
						<div class="col-md-2">
							<button class="btn btn-primary ">Filter Report	</button>
						</div>
					</form>
				</div>
				<div class="panel-body">
					<div class="row" style="margin-top:20px;">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<table id="datatable-button" class="table table-custom table-bordered">
								<thead>
									<tr>
										<th class="whiter">No</th>
										<th class="whiter">Kode Order</th>
										<th class="whiter">Cabang</th>
										<th class="whiter">Nomor HP</th>
										<th class="whiter">Email</th>
										<th class="whiter">Total Harga</th>
										<th class="whiter">Status</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 0; foreach ($order as $key => $value) { $no++;?>
									<tr>
										<td><?=$no?></td>
										<td><?=$value['orderCode']?></td>
										<td><?=$value['branchName']?></td>
										<td><?=$value['orderContact']?></td>
										<td><?=$value['orderEmail']?></td>
										<td><?=konversi_uang($value['orderPrice'])?></td>
										<td><?=$value['orderStatus']?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable-button').DataTable( {
			dom: 'Bfrtip',
			buttons: [
			{
				extend:    'copyHtml5',
				text:      '<i class="fa fa-files-o"></i> Copy',
				titleAttr: 'Copy'
			},
			{
				extend:    'csvHtml5',
				text:      '<i class="fa fa-file-text-o"></i> Simpan file Excel',
				titleAttr: 'CSV'
			},
			{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> Print data',
				titleAttr: 'Print'
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fa fa-file-pdf-o"></i> Simpan file PDF',
				titleAttr: 'PDF'
			}
			]
		} );
	} );
</script>