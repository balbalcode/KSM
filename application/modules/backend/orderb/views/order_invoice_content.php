<div class="container">

	<?= getBread() ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-border panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title"> Order Payment </h4>
				</div>
				<div class="panel-body">
					<div class="row" style="margin-top:20px;">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<table id="datatable" class="table table-custom table-bordered">
						<thead>
							<tr>
								<th class="whiter">No</th>
								<th class="whiter">Kode Order</th>
								<th class="whiter">Nama</th>
								<th class="whiter">Email</th>
								<th class="whiter">Bukti Transfer</th>
								
							</tr>
						</thead>
						<tbody>
							<?php $no = 0; foreach ($invoice as $key => $value) { $no++;?>
							<tr>
								<td><?=$no?></td>
								<td><?=$value['orderInvoice']?></td>
								<td><?=$value['orderPaymentName']?></td>
								<td><?=$value['orderPaymentEmail']?></td>
								<td class="text-center"><a class="btn btn-primary btn-xs" target="_blank" href="<?=base_url().'assets/uploads/order/payment/'. $value['orderPayment']?>">Lihat Gambar</a></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>