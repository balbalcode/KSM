<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orderb_model extends CI_Model {

	public function get_filter_order($branchId='',$orderStatus='')
	{
		if ($branchId) {
			$param['branchId'] = $branchId;
		}
		
		$param['orderStatus'] = $orderStatus;

		return $this->model->join('order','order.*,master_branch.branchName',array(array('table' => 'master_branch' , 'parameter' => 'order.branchId=master_branch.branchId')), $param, 'createDate DESC');
	}

	public function update_order($data='')
	{
		$this->db->where('orderId', $data['orderId']);
		$this->db->update('order', $data);

		return true;
	}

	public function update_stock($orderId='')
	{
		$productId =  $this->get_product_id($orderId);
		$stock 	   =  $this->get_stock($productId);

		$newStock =  array(
			'productStock' 	=> $stock-1,
			'updateDate'   	=> date('Y-m-d h:i:s'),
			'updateBy'		=> $this->session->userdata('usernameUser')
		);

		$this->db->where('productId', $productId);
		$this->db->update('product', $newStock);

		return true;	
	}

	public function get_product_id($orderId='')
	{
		$this->db->select('productId');
		$this->db->from('order');
		$this->db->where('orderId', $orderId);

		$getProduct = $this->db->get()->result();

		return $getProduct[0]->productId;
	}

	public function get_stock($productId='')
	{
		$this->db->select('productStock');
		$this->db->from('product');
		$this->db->where('productId', $productId);

		$getStock = $this->db->get()->result();

		return $getStock[0]->productStock;
	}

}

/* End of file orderb_model.php */
/* Location: ./application/models/orderb_model.php */