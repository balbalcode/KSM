
<div class="container">
	<?= getBread() ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel  panel-border panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Cabang Toko </h3>
				</div>
				<div class="panel-body">
					<div class="row"> 
						<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>/save">
								<input type="hidden" name="branchId" class="form-control" value="<?php echo ($data) ? $data[0]['branchId'] : "" ?>">
								<?php echo input_text_group('branchName','Nama',(@$data[0]['branchName']) ? @$data[0]['branchName'] : set_value('branchName'),' Nama Cabang','') ?>		
								<?php echo input_text_group('branchXAxis','Koordinat X',(@$data[0]['branchXAxis']) ? @$data[0]['branchXAxis'] : set_value('branchXAxis'),' Koordinat X Map') ?>
								<?php echo input_text_group('branchYAxis','Koordinat Y',(@$data[0]['branchYAxis']) ? @$data[0]['branchYAxis'] : set_value('branchYAxis'),' Koordinat X Map','') ?>
								<?php echo input_text_group('branchLink','Link Map',(@$data[0]['branchLink']) ? @$data[0]['branchLink'] : set_value('branchLink'),'Link Map') ?>
								<?php echo input_textarea_group('branchAddress','Alamat Cabang',(@$data[0]['branchAddress']) ? @$data[0]['branchAddress'] : set_value('branchAddress'),' Alamat Cabang') ?>
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
									&nbsp;
									<a href="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>" class="btn btn-inverse"><i class="fa fa-times"></i> Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>