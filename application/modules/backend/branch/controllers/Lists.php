<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lists extends MY_Controller {

	public function index()
	{
		if(hak_akses('view') === FALSE){
			show_errPrivilege();
			exit();
		}
		$data['data'] = $this->model->get('master_branch');
		$data['content'] = 'lists_content';
		$this->load->view('backend/main',$data,FALSE);
	}

	public function add($id='',$param='')
	{
		$data['content'] 		= 'lists_add';
		$data['data'] 		  	= $this->model->get_where('master_branch',array('branchId'=> $id));
		$this->load->view('backend/main',$data,FALSE);
	}

	public function save()
	{
		$post = $this->input->post();
		if (@$post['branchId']) {
			if(hak_akses('update') === FALSE){
				show_errPrivilege();
				exit();
			}
			$post['updateDate'] = date('Y-m-d H:i:s');
			$post['updateBy'] = $this->session->userdata('usernameUser');
			$this->model->update_data('master_branch',$post,array('branchId'=>$post['branchId']));
		} else {
			if(hak_akses('create') === FALSE){
				show_errPrivilege();
				exit();
			}
			$post['createDate'] = date('Y-m-d H:i:s');
			$post['createBy'] = $this->session->userdata('usernameUser');
			$this->model->insert_data('master_branch',$post);
		}
		redirect(getModule().'/'.getController());
	}

	public function delete($id="")
	{
		$this->model->delete_data('master_branch', 'branchId='.$id);	
	}

}

/* End of file Slider.php */
/* Location: ./application/modules/setting/controllers/Slider.php */