
<div class="container" style="margin-bottom: 20px">
	<div class="row <?=@$sectionBar?>" >
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-6 col-lg-3">
					<a href="<?=base_url().'orderb/complete'?>">
						<div class="mini-stat clearfix bx-shadow bg-white">
							<span class="mini-stat-icon bg-primary"><i class="ion-ios7-cart"></i></span>
							<div class="mini-stat-info text-right text-dark">
								<span class="counter text-dark"><?=@$orderCount?></span>
								Order Success
							</div>
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-lg-3">
					<a href="<?=base_url().'products/lists'?>">
						<div class="mini-stat clearfix bx-shadow bg-white">
							<span class="mini-stat-icon bg-primary"><i class="fa fa-cubes"></i></span>
							<div class="mini-stat-info text-right text-dark">
								<span class="counter text-dark"><?=@$productCount?></span>
								Total Product
							</div>
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-lg-3">
					<a href="<?=base_url().'branch/lists'?>">
						<div class="mini-stat clearfix bx-shadow bg-white">
							<span class="mini-stat-icon bg-primary"><i class="fa fa-map-marker"></i></span>
							<div class="mini-stat-info text-right text-dark">
								<span class="counter text-dark"><?=@$branchCount?></span>
								Cabang
							</div>
						</div>
					</a>
				</div>
				
				<div class="col-sm-6 col-lg-3">
					<a href="<?=base_url().'setting/user'?>">
						<div class="mini-stat clearfix bx-shadow bg-white">
							<span class="mini-stat-icon bg-primary"><i class="ion-android-contacts"></i></span>
							<div class="mini-stat-info text-right text-dark">
								<span class="counter text-dark"><?=@$adminCount?></span>
								Admin
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-sm-9" id="chart"></div>
	</div>

	
	
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-border panel-primary">
				<div class="panel-heading" style="border-radius: 0">
					<h4 class="panel-title">Shortcut Order Incoming</h4>
				</div>
				<div class="panel-body">
					<table id="datatable" class="table table-custom table-bordered">
						<thead>
							<tr>
								<th class="whiter">No</th>
								<th class="whiter">Kode Order</th>
								<th class="whiter">Nomor HP</th>
								<th class="whiter">Email</th>
								<th class="whiter">Total Harga</th>
								<th class="whiter">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 0; foreach ($order as $key => $value) { $no++;?>
							<tr>
								<td><?=$no?></td>
								<td><?=$value['orderCode']?></td>
								<td><?=$value['orderContact']?></td>
								<td><?=$value['orderEmail']?></td>
								<td class="text-right">Rp. <?=konversi_uang($value['orderPrice'])?></td>
								<td class="text-center">
									<a href="<?php echo base_url() ?>orderb/incoming/approve/<?php echo $value['orderId'] ?>">
										<button  data-toggle="tooltip" data-title="Ongoing Order" data-placement="bottom"   class="<?=@$hiddenApprove?>  btn btn-icon waves-effect waves-light btn-success btn-xs m-b-5" data-attr="<?= $value['orderId'] ?>"><i class="fa fa-check"></i> </button>
									</a>
									<a href="<?php echo base_url() ?>orderb/incoming/detail/<?php echo $value['orderId'] ?>">
										<button data-toggle="tooltip" data-title="Detail" data-placement="bottom"   class="btn btn-icon waves-effect waves-light btn-inverse btn-xs m-b-5" data-attr="<?= $value['orderId'] ?>"><i class="fa fa-eye"></i> </button>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
					
				</div>
			</div>
		</div>
		
	</div>

</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		date = (new Date()).getFullYear();
		Highcharts.chart('chart', {
			chart: {
				type: 'area'
			},
			title: {
				text: 'Penjualan Mesin Tahun ' + date
			},
			xAxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
				'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
			},
			yAxis: {
				title: {
					text: 'Jumlah Mesin'
				},
			},
			tooltip: {
				pointFormat: '{series.name} Terjual <b>{point.y:,.0f}</b><br/> '
			},
			plotOptions: {
				spline: {
					marker: {
						radius: 4,
						lineColor: '#666666',
						lineWidth: 1
					}
				}
			},
			series: [{
				name: 'Mesin',
				data: [<?=$chart?>]

			}]
		});
	});
</script>