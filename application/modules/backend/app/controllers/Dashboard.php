<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function index()
	{
		$branch =  $this->session->userdata('branchUser');
		
		if ($branch) {
			$data = array(
				'sectionBar'   => 'hidden'
			);
		}
		else{
			$data = array(
				'sectionBar'   => '',
				'adminCount'   => sizeof($this->model->get_where('user',array('roleUser'=>'3'))),
				'branchCount'  => sizeof($this->model->get('master_branch')),
				'productCount' => sizeof($this->model->get('product')),
				'orderCount'   => sizeof($this->model->get('order')),
				'chart'   	   => $this->query_chart(),
			);
		}
		$data['order']   = $this->get_filter_order($this->session->userdata('branchUser'));
		$data['content'] = 'dashboard_content';
		
		
		$this->load->view('backend/main',$data,FALSE);
	}

	public function get_filter_order($branchId='')
	{
		$param = array(
			'orderStatus'=> 'Incoming'
		);
		if ($branchId) {
			$param['branchId'] = $branchId;
		}
		return $this->model->get_where('order', $param);
	}

	public function query_chart($value='')
	{
		$data = $this->model->get_where('order', array('orderStatus'=>'Complete'), '','','','months','count(orderId) as soldMachine, MONTH(createDate) as months');
		
		$soldMachine = array(
			0 => '0',
			1 => '0',
			2 => '0',
			3 => '0',
			4 => '0',
			5 => '0',
			6 => '0',
			7 => '0',
			8 => '0',
			9 => '0',
			10 => '0',
			11 => '0',
		);
		foreach ($data as $key => $value) {
			$soldMachine[$value['months']-1] = $value['soldMachine'];	
		}
		
		return implode(",", $soldMachine);
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */