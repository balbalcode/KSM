<div class="container">

	<?= getBread() ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-border panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title"> Master Section</h4>
				</div>
				<div class="panel-body">
					<div class="row" style="margin-top:20px;">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<table id="datatable" class="table table-custom table-bordered">
								<thead>
									<tr>
										<th class="text-center">Section</th>
										<th class="text-center">Caption</th>
										<th class="text-center">Detail</th>
										<?php if(hak_akses('update') || hak_akses('delete')){ ?>
										<th class="text-center">Action</th>
										<?php } ?>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="vertical-align:middle;text-align: ;">Review</td>
										<td style="vertical-align:middle;text-align: center;"><?= @$valueReview[0]['reviewCaption'] ?></td>
										<td style="vertical-align:middle;"><?= @$valueReview[0]['reviewDetail'] ?></td>
										<?php if(hak_akses('update') || hak_akses('delete')){ ?>
										<td style="vertical-align:middle;" class="text-center">
											<?php if(hak_akses('update')){ ?>
											<a href="<?php echo base_url().getModule() ?>/<?php echo getController() ?>/detail/review">
												<button class="btn btn-icon waves-effect waves-light btn-inverse btn-xs m-b-5"><i class="fa fa-pencil"></i></button>
											</a>
											<?php } ?>
										</td>
										<?php } ?>
									</tr>
									<tr>
										<td style="vertical-align:middle;">Driver</td>
										<td style="vertical-align:middle;text-align: center;"><?= @$valueDriver[0]['driverCaption'] ?></td>
										<td style="vertical-align:middle;"><?= @$valueDriver[0]['driverDetail'] ?></td>
										<?php if(hak_akses('update') || hak_akses('delete')){ ?>
										<td style="vertical-align:middle;" class="text-center">
											<?php if(hak_akses('update')){ ?>
											<a href="<?php echo base_url().getModule() ?>/<?php echo getController() ?>/detail/driver">
												<button class="btn btn-icon waves-effect waves-light btn-inverse btn-xs m-b-5"><i class="fa fa-pencil"></i></button>
											</a>
											<?php } ?>
										</td>
										<?php } ?>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>