
<div class="container">
	<?= getBread() ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel  panel-border panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Driver </h3>
				</div>
				<div class="panel-body">
					<div class="row"> 
						<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>/save/driver">
								<?php echo input_text_group('driverCaption','Caption',(@$data[0]['driverCaption']) ? @$data[0]['driverCaption'] :  '' ,' Caption driver','') ?>		
								<?php echo input_textarea_group('driverDetail','Detail',(@$data[0]['driverDetail']) ? @$data[0]['driverDetail'] : set_value('driverDetail'),'Detail ','') ?>
								<?php echo input_file_image_group('driverImage','Gambar',!empty(@$data[0]['driverImage']) ? base_url().'assets/uploads/driver/thumb/'. @$data[0]['driverImage'] : '' ,   array('data-title' => 'Hapus gambar','data-desc' => 'Apakah anda yakin ingin menghapus gambar ini?','data-confirm' => 'Berhasil di hapus','data-route' => base_url(getModule()."/".getController()."/detail/driver/delete")), 'required')?>			
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
									&nbsp;
									<a href="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>" class="btn btn-inverse"><i class="fa fa-times"></i> Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>