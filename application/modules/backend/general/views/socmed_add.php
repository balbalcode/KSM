
<div class="container">
	<?= getBread() ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel  panel-border panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Sosial Media </h3>
				</div>
				<div class="panel-body">
					<div class="row"> 
						<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>/save">
								<input type="hidden" name="socmedId" class="form-control" value="<?php echo ($data) ? $data[0]['socmedId'] : "" ?>">
								<?php echo input_text_group('socmedName','Nama',(@$data[0]['socmedName']) ? @$data[0]['socmedName'] : set_value('socmedName'),' Nama Sosial Media','') ?>
								<?php echo input_icon_group('socmedIcon','Icon',(@$data[0]['socmedIcon']) ? @$data[0]['socmedIcon'] : set_value('socmedIcon'),'Klik disini...') ?>				
								<?php echo input_text_group('target','Link',(@$data[0]['target']) ? @$data[0]['target'] : set_value('Link'),' Link Sosial Media','') ?>
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
									&nbsp;
									<a href="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>" class="btn btn-inverse"><i class="fa fa-times"></i> Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>