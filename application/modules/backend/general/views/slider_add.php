
<div class="container">
	<?= getBread() ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel  panel-border panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Slider </h3>
				</div>
				<div class="panel-body">
					<div class="row"> 
						<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>/save">
								<input type="hidden" name="sliderId" class="form-control" value="<?php echo ($data) ? $data[0]['sliderId'] : "" ?>">
								<?php echo input_text_group('sliderName','Nama',(@$data[0]['sliderName']) ? @$data[0]['sliderName'] : set_value('sliderName'),'Nama Slider ','') ?>
								<?php echo input_file_image_group('sliderImage','Gambar',!empty(@$data[0]['sliderImage']) ? base_url().'assets/uploads/slider/thumb/'. @$data[0]['sliderImage'] : '' ,   array('data-title' => 'Hapus gambar','data-desc' => 'Apakah anda yakin ingin menghapus gambar ini?','data-confirm' => 'Berhasil di hapus','data-route' => base_url(getModule()."/".getController()."/add/".@$data[0]['sliderId']."/delete")), 'required')?>				
								<?php echo input_textarea_group('sliderDetail','Detail',(@$data[0]['sliderDetail']) ? @$data[0]['sliderDetail'] : set_value('sliderDetail'),'Detail Slider','') ?>
								<?php echo input_radio_group('sliderStatus','Status',array('t'=>'Active','f'=>'Not Active'),(@$data[0]['sliderStatus']) ? @$data[0]['sliderStatus'] : 't' ,'required') ?>
													
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
									&nbsp;
									<a href="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>" class="btn btn-inverse"><i class="fa fa-times"></i> Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>