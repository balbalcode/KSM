<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section extends MY_Controller {

	public function index()
	{
		if(hak_akses('view') === FALSE){
			show_errPrivilege();
			exit();
		}
		$data['valueDriver'] = $this->model->get('driver');
		$data['valueReview'] = $this->model->get('review');
		$data['content'] = 'section_content';
		$this->load->view('backend/main',$data,FALSE);
	}

	public function detail($module,$param='')
	{
		if ($param!="" && $param == "delete") {
			$this->load->library('uploads');
			$getImage = $this->model->get('driver');
			
			$photo = $this->uploads->upload_image('driverImage','assets/uploads/driver');
			unlink('assets/uploads/driver/'.$getImage[0]['driverImage']);
			unlink('assets/uploads/driver/thumb/'.$getImage[0]['driverImage']);
			
			$post['driverImage'] = '';
			$this->db->update($module,$post);		
			redirect(base_url(getModule().'/'.getController().'/detail/driver/'.$id),'refresh');
		}
		$data['content'] 		= $module.'_add';
		$data['data'] 		  	= $this->model->get($module);
		$this->load->view('backend/main',$data,FALSE);
	}

	public function save($table)
	{
		$post = $this->input->post();
		if ($table=='driver') {
			if (@$_FILES['driverImage']['name']!='') 
			{
				$this->load->library('uploads');
				$dir = "assets/uploads/driver/";
				$data = $this->uploads->upload_image('driverImage',$dir);
				$name = $data['file_name'];
				$this->uploads->resize_image($dir.$name,$dir.'thumb/','525','350',''.$name);
				if(@$post['driverImage']=="")
				{
					$post['driverImage'] = $name; 
				}

			}
		}
		if(hak_akses('update') === FALSE){
			show_errPrivilege();
			exit();
		}
		$post['updateDate'] = date('Y-m-d H:i:s');
		$post['updateBy'] = $this->session->userdata('usernameUser');
		$this->db->update($table,$post);		
		redirect(getModule().'/'.getController());
	}

}

/* End of file driver.php */
/* Location: ./application/modules/setting/controllers/driver.php */