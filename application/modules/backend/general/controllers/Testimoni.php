<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni extends MY_Controller {

	public function index()
	{
		if(hak_akses('view') === FALSE){
			show_errPrivilege();
			exit();
		}
		$data['data'] = $this->model->get('testimonials');
		$data['content'] = 'testimoni_content';
		$this->load->view('backend/main',$data,FALSE);
	}

	public function add($id='',$param='')
	{

		if ($param!="" && $param == "delete") {
			$this->load->library('uploads');
			$getLogo = $this->model->get_where('testimonials',array('testimoniId'=>$id));
			
			$photo = $this->uploads->upload_image('testimoniUserPicture','assets/uploads/testimoni');
			unlink('assets/uploads/testimoni/'.$getLogo[0]['testimoniUserPicture']);
			unlink('assets/uploads/testimoni/thumb/'.$getLogo[0]['testimoniUserPicture']);
			
			$post['testimoniUserPicture'] = $photo['file_name'];
			$this->model->update_data('testimonials',$post,array('testimoniId'=>$id));
			redirect(base_url(getModule().'/'.getController().'/add/'.$id),'refresh');
		}

		$data['content'] 		= 'testimoni_add';
		$data['data'] 		  	= $this->model->get_where('testimonials',array('testimoniId'=> $id));
		$this->load->view('backend/main',$data,FALSE);
	}

	public function save()
	{
		$post = $this->input->post();

		if (@$_FILES['testimoniUserPicture']['name']!='') 
		{
			$this->load->library('uploads');
			$dir  = "assets/uploads/testimoni/";
			$data = $this->uploads->upload_image('testimoniUserPicture',$dir);
			$name = $data['file_name'];
			$this->uploads->resize_image($dir.$name,$dir.'thumb/','525','350',''.$name);
			if(@$post['testimoniUserPicture']=="")
			{
				$post['testimoniUserPicture'] = $name; 
			}

		}

		if (@$post['testimoniId']) {
			if(hak_akses('update') === FALSE){
				show_errPrivilege();
				exit();
			}
			$post['updateDate'] = date('Y-m-d H:i:s');
			$post['updateBy'] = $this->session->userdata('usernameUser');
			$this->model->update_data('testimonials',$post,array('testimoniId'=>$post['testimoniId']));
		} else {
			if(hak_akses('create') === FALSE){
				show_errPrivilege();
				exit();
			}
			$post['createDate'] = date('Y-m-d H:i:s');
			$post['createBy'] = $this->session->userdata('usernameUser');
			$this->model->insert_data('testimonials',$post);
		}
		redirect(getModule().'/'.getController());
	}

	public function delete($id="")
	{
		$getLogo = $this->model->get_where('testimonials',array('testimoniId'=>$id));
		$this->model->delete_data('testimonials', 'testimoniId='.$id);
		unlink('assets/uploads/testimoni/'.$getLogo[0]['testimoniUserPicture']);
		unlink('assets/uploads/testimoni/thumb/'.$getLogo[0]['testimoniUserPicture']);
	}

}

/* End of file Slider.php */
/* Location: ./application/modules/setting/controllers/Slider.php */