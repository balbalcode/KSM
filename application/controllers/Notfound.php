<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends CI_Controller {

	public function index()
	{
		$data = array(
			'socmed'     	=> $this->db->get('socmed')->result(),
			'header'		=> "none",
			'content'		=> 'frontend/pages/404',
			
		);
		$this->load->view('frontend/layout/main', $data, FALSE);	
	}

	public function product()
	{
		$data = array(
			'socmed'     	=> $this->db->get('socmed')->result(),
			'header'		=> "none",
			'content'		=> 'frontend/pages/product_not_found',
			
		);
		$this->load->view('frontend/layout/main', $data, FALSE);	
	}

}

/* End of file 404.php */
/* Location: ./application/controllers/404.php */