<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	public function index()
	{
		
	}

	public function email()
	{
		$this->load->library('mail');
		$this->mail->send('zizcode.exporadev@gmail.com', 'Subject', 'Bismillah');
	}

}

/* End of file Test.php */
/* Location: ./application/controllers/Test.php */