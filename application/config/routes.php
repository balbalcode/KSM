<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'content/home';
$route['404_override'] = 'Notfound';
$route['translate_uri_dashes'] = FALSE;

// ROUTE FRONTEND
$route['product/(:any)'] = 'product/index/$1';
$route['order/post'] = 'order/post';
$route['order/struck/(:any)'] = 'order/struck/$1';
$route['order/payment'] = 'order/payment';
$route['order/payment/(:any)'] = 'order/payment/$1';
$route['order/transaction'] = 'order/transaction';
$route['order/(:any)'] = 'order/index/$1';
// $route['satws/(:any)'] = 'news/detail/$1';
// $route['kauan/(:any)'] = 'content/satuan/$1';
// $route['gallery/(:any)'] = 'gallery/index/$1';
// $route['page/(:any)'] = 'page/index/$1';
// $route['bulletin/(:any)'] = 'bulletin/detail/$1';

//MODULE APP
$route['app'] = 'app/login';
$route['app/login'] = 'app/login';
$route['app/logout'] = 'app/logout';
$route['app/profile'] = 'app/profile';

$route['dashboard'] = 'app/dashboard';