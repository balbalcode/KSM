<footer style="z-index: 99;position: relative;">
		<div class="footer-area">
			<!-- footer top starts -->
			<div class="light-bg padding-medium footer-top">
				<div class="container">
					<div class="row xs-text-center">
						<div class="col-md-7 col-sm-12  ">
							<div class="footer-widget">
								<h4 class="widget-title">WHO WE ARE</h4>
								<div class="widget-content">
									<p style="line-height: 1.5em">
									Perusahaan Nasional yang bergerak dibidang jasa penyediaan dan penyewaan mesin cetak Mutifungsi serta bahan pakainya.
									KSM memiliki tenaga ahli dari generasi muda yang mempunyai semangat, kreativitas, dedikasi dan inovasi yang sangat tinggi
									dengan bimbingan dan arahan dari para ahli dibidangnya.<br><br>
									<b style="font-size: 0.8em">
									PT. KREASI SARANA MANDIRI  <br> berdiri sejak 1 agustus 2010 s/d saat ini
									.</b>
									</p>
								</div>
							</div><!-- /End footer-widget -->
						</div>						
						<!-- single widget -->
						<div class="col-md-offset-1 col-md-2 col-sm-6 hidden-xs hidden-sm">
							<div class="footer-widget">
								<h4 class="widget-title">Navigation</h4>
								<ul class="widget-menu">
									<li><a href="<?=base_url().'#home'?>">Home</a>
									<li><a href="<?=base_url().'#feature'?>">Why Us</a>
									<li><a href="<?=base_url().'#overview'?>">Overview</a>
									<li><a href="<?=base_url().'#feedback'?>">Testimonials</a>
									<li><a href="<?=base_url().'#driver'?>">Driver</a>
									<li><a href="<?=base_url().'#contact'?>">Contact Us</a>
								</ul>
							</div><!-- /End footer-widget -->
						</div>
						<!-- /single widget -->
						<!-- single widget -->
						<div class="col-md-2 col-sm-6">
							<div class="footer-widget">
								<h4 class="widget-title">Order and Transaction</h4>
									<ul class="widget-menu">
										<li><a href="<?=base_url().'order/payment'?>">Upload Transaction</a></li>
									
								</ul>
							</div><!-- /End footer-widget -->
						</div>
						<!-- /single widget -->
						<!-- single widget -->
						<!-- /single widget -->
						<!-- single widget -->
						
						<!-- /single widget -->
					</div><!-- /End row -->
				</div><!-- /End container -->
			</div>
			<!-- /footer top ends -->
			<!-- footer bottom starts -->
			<div class="light-bg-1 footer-bottom">
				<div class="container">
					<div class="row xs-text-center">
						<div class="col-sm-12">
							<div class="text-center footer-left">
								<p class="copyright-text">All Right Reserved &copy; 2018 | KSM</p>
							</div>
						</div><!-- /End col -->
					</div><!-- /End row -->
					<div class="row">					
						<!-- Starts Fixed Social icons -->
						<i class="icofont icofont-arrow-right social-trigger hidden-xs"></i>
						<div class="fixed-social-bar">
							<ul class="text-center social-icons">
								<?php foreach ($socmed as $key => $value) {  ?>
								<li style="height: 35px;width: 35px;text-align: center;background: #ff647a;border-radius: 17px;"><a href="<?=$value->target?>" target="_blank"><i class=" fa <?=$value->socmedIcon?> circled-icon"></i></a></li>
								<?php } 		?>
							</ul>
						</div>
						<!-- /Ends Fixed Social icons -->
					</div><!-- /End row -->
				</div><!-- /End container -->
			</div>
			<!-- /footer bottom starts -->
		</div><!-- /End footer-area -->
	</footer>