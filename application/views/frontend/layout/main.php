<!DOCTYPE html>
<html>
<head>

	<?php
	if (isset($title)) {
		$title = $title." - Korbrimob Polri | Jiwa Ragaku Demi Kemanusiaan";
	}else{
		$title = "KSM | CV. Kreasi Sukses Mandiri";
	}
	?>

	<title><?= $title ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Perusahaan Nasional yang bergerak dibidang jasa penyediaan dan penyewaan mesin cetak Mutifungsi serta bahan pakainya.Memiliki tenaga ahli dari generasi muda yang mempunyai semangat, kreativitas, dedikasi dan inovasi yang sangat tinggi dengan bimbingan dan arahan dari para ahli dibidangnya.">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<meta name="author" content="CV. Kreasi Sukses Mandiri">

	<link rel="shortcut icon" href="<?= base_url('assets/img/logo/icon.png') ?>">

	<script type="text/javascript">
		var BASE_URL = "<?= base_url() ?>";
	</script>

	<?php

	if (isset($other_css)) {
		echo load_css($other_css); 
	}

	$multiple_css = array(
		'frontend/bootstrap/css/bootstrap.css',
		'frontend/css/plugins/icofont.css',
		'frontend/css/plugins/animate.css',
		'frontend/css/plugins/owl.carousel.min.css',
		'frontend/css/plugins/magnific-popup.css',
		'frontend/css/plugins/common.css',
		'frontend/css/plugins/style-3.css',
		'frontend/css/plugins/responsive-3.css',
		'frontend/css/plugins/style.css',
		'frontend/material-icons/material-icons.css',
		'frontend/plugins/bs-select/select2.css',
		'frontend/plugins/bs-select/select2.bootstrap.css',
		'frontend/font-awesome/css/font-awesome.min.css',
		'frontend/plugins/slick/slick.css',
		'frontend/plugins/slick/slick-theme.css',
		'backend/plugins/sweetalert/dist/sweetalert.css'
	);

	echo load_css($multiple_css);

	$multiple_js = array(
		'frontend/js/plugins/modernizr-2.8.3-respond-1.4.2.min.js',
		'frontend/js/jquery-2.1.3.min.js',
		'frontend/js/jquery.mousewheel.min.js',
		'frontend/plugins/bs-select/select2.js',
		'backend/plugins/sweetalert/dist/sweetalert.min.js',
		'frontend/js/jquery-scrolltofixed-min.js',
		'frontend/plugins/slick/slick.js'
	);

	echo load_js($multiple_js);

	if (isset($other_js_top)) {
		echo load_js($other_js_top); 
	}

	?>
	<style type="text/css">	

	.slick-prev:before,
	.slick-next:before {
		color: #ec3851;
	}
</style>
</head>
<body style="overflow-x: hidden;">
	<?php

	if(@$header!='none'){
		echo $this->load->view($header,'',TRUE);
	}
	elseif (@$header) {
		
	}
	else{
		echo $this->load->view('frontend/layout/header','',TRUE);		
	}

	echo $this->load->view($content,'',TRUE);
	
	if(!@$non_footer){
		echo $this->load->view('frontend/layout/footer','',TRUE);
	}

	$multiple_js = array(
		'frontend/bootstrap/js/bootstrap.min.js',
		'frontend/js/plugins/owl.carousel.min.js',
		'frontend/js/plugins/jquery.magnific-popup.min.js',
		'frontend/js/plugins/jquery.easypiechart.min.js',
		'frontend/js/plugins/wow.min.js',
		'frontend/js/plugins/main-3.js'
	);
	
	echo load_js($multiple_js);

	if (isset($other_js)) {
		echo load_js($other_js); 
	}

	?>

</body>
<script type="text/javascript">
	$(document).ready(function(){
		$("a.smooth").on('click', function(event) {
			if (this.hash !== "") {
				event.preventDefault();
				var hash = this.hash;
				$('html, body').animate({
					scrollTop: $(hash).offset().top
				}, 800, function(){
					window.location.hash = hash;
				});
			} 
		});
	});
</script>
</html>