<style type="text/css">
h1{
	font-size: 8em;
}	
h5{
	font-size: 2em;
}
</style>

<section class="main-content bg-white" style="margin: 100px">
	<div class="container-fluid">

		<div class="col-md-12 text-center">
			<h1>404 !</h1>
			<h6 style="font-size: 1.3em">Maaf, Product yang anda cari tidak ada dalam daftar product kami.</h6>
			<br clear="all">
			<a href="<?= base_url().'product' ?>" >
				<button  class="btn btn-round btn-red btn-submit">Back To Product List</button>
			</a>
		</div>

	</div>
</section>