<style type="text/css">
h1{
	font-size: 8em;
}	
h5{
	font-size: 2em;
}
</style>

<section class="main-content bg-white" style="margin: 100px">
	<div class="container-fluid">

		<div class="col-md-12 text-center">
			<h1>404 !</h1>
			<h5>Sorry, page not found</h5>
			<br clear="all">
			<a href="<?= base_url() ?>" >
				<button id="send_message"  class="btn btn-round btn-red btn-submit">Back To Home</button>
				
			</a>
		</div>

	</div>
</section>