<?php
class Mail
{
  function send($from="",$to="",$subject="",$message="",$attach="",$errors="")
  { 
    $ci =& get_instance();

    $config = array(
      'protocol'     => 'smtp',
      'smtp_host'    => 'mail.ksm-in.com',
      'smtp_port'    => '587',
      'smtp_user'    => 'noreply@ksm-in.com',
      'smtp_pass'    => 'n0r3ply@#',
      'mailtype'     => 'html', 
      'charset'      => 'UTF-8',
      'smtp_timeout' => '5'
    );

    $ci->load->library('email', $config);
    $ci->email->set_newline("\r\n");
    $ci->email->from($from, "CV. Kreasi Sukses Mandiri");
    $ci->email->to($to);
    $ci->email->subject($subject);
    $ci->email->message($message);

    if ($attach) {
      $ci->email->attach($attach);
    }
    if (!$ci->email->send()) {
      print_r($ci->email->print_debugger());
      return "FAILED";       
    } else {
     return "SUCCESS";
   }

 }
}